import React, { useState,useEffect } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { createProficienciesList } from './createProficienciesList';
import { createLanguagesList } from './createLanguagesList';
import {
    getClasses,
    getRaces,
    getProficiencies,
    getLanguages
} from "../api/api"
import "../styles/BasicInfo.css" //Fixed the css problems here

const basicInfoSchema = Yup.object().shape({
    chosenName: Yup.string().notRequired(),
    chosenClass: Yup.string().required('Class is required'),
    chosenRace: Yup.string().required('Race is required'),
  });

const BasicInfo = ()  => {
    const [getName, setName] = useState("");
    const [classOptions,setClass] = useState([]);
    const [raceOptions,setRace] = useState([]);
    const [getClass] = useState("");
    const [getRace] = useState("");
    const [proficiencies,setProf] = useState([]);
    const [languages,setLang] = useState([]);

    const handleSubmit = async(e) => {
        const chosenClass = e.chosenClass.name;
        const chosenRace = e.chosenRace.name;
        let prof = await getProficiencies(chosenClass)
        let lang = await getLanguages(chosenRace)

        //Create two arrays so we can convert this from an array of objects to an array of values
        let langArray =[] 
        let profArray = []
        //Map the objects and get what we need
        lang.map(item=>{
            langArray.push(item.name)
            return(null)
        })
        prof.map(item=>{
            profArray.push(item.name)
            return(null)
        })
        //Finally set it to state
        setLang(langArray)
        setProf(profArray)
    }

    const setCharacterName = e => {
        setName(e);
    };

    async function initialize(){  //Populate classes and races here
        let Class = await getClasses()
        let Race = await getRaces()
        setClass(Class.results)
        setRace(Race.results)
    }
    useEffect(() => {
        initialize()
    }, []);
        
    return(
        <>
            <h2>Choose name, race & class</h2>
            <Formik>
                <Field name="chosenName" render={({field}) =>
                    <span className='p-float-label' style={{ marginTop: '1rem' }}>
                        <InputText id='chosenName' {...field}  value={getName} onChange={e => {
                            setCharacterName(e.target.value)}} style={{ width: '100%' }}/>
                        <label htmlFor='chosenName'>Enter Name</label>
                    </span>
                }/>
            </Formik>
            <Formik
                initialValues={{
                  chosenClass: '',
                  chosenRace: ''
                }}
                validationSchema={basicInfoSchema}
                onSubmit={e => handleSubmit(e)}
                render={() => (
                    <Form>
                        <div className="dropdownFormContainer" >
                            <Field name="chosenRace" render={({field}) =>
                            <Dropdown
                                {...field}
                                className="dropdownFormElement element1"
                                optionLabel="name"
                                options={raceOptions}
                                placeholder="Select D&D Race"/>}/>
                            <ErrorMessage name='chosenRace'/>

                            <Field name="chosenClass" render={({field}) =>
                            <Dropdown
                                {...field}
                                className="dropdownFormElement element2"
                                optionLabel="name"
                                options={classOptions}
                                placeholder="Select D&D Class"/>}/>
                            <ErrorMessage name='chosenClass'/>
                            <Button 
                                label="Select" 
                                className="dropdownFormElement dropdownFormButton p-button-raised element3" 
                                style={{ 
                                     
                                }
                            }/>
                        </div>
                    </Form>
                )}
            />
            <div>
                <h3 className="NameRaceClass">{getName} {(getName && getRace && getClass) ? "the" : null} {getRace} {getClass}</h3>
            </div>
            <div>
                {createProficienciesList(proficiencies)}
            </div>
            <div>
                {createLanguagesList(languages)}
            </div>
        </>
        
    );
};

export default BasicInfo;
