export const getClasses = async() => {
    let response = await fetch('https://www.dnd5eapi.co/api/classes')
    let res = await response.json()
    return await res
}
export const getRaces = async() => {
    let response = await fetch('https://www.dnd5eapi.co/api/races')
    let res = await response.json()
    return await res
}
export const getProficiencies = async(classType) => {
    let url = 'https://www.dnd5eapi.co/api/classes/'+classType.toLowerCase()
    let response = await fetch(url)
    let res = await response.json()
    res = await res.proficiencies
    return await res
}
export const getLanguages = async(race) => {
    let url = 'https://www.dnd5eapi.co/api/races/'+race.toLowerCase()
    let response = await fetch(url)
    let res = await response.json()
    res = await res.languages
    return await res
}