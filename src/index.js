import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
serviceWorker.unregister();

//To fix build npm install --ignore-scripts was run 
//Formik was not installed afterward so that was installed with npm afterward